var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
/*var pool = mysql.createPool({
  connectionLimit: 1,
  host: "localhost",
  user: "root",
  password: "123",
  database: "test2",
  debug: false,
  multipleStatements: true
});*/
var pool = mysql.createPool({
    connectionLimit: 2,
    host: "mysql.stud.iie.ntnu.no",
    user: "nilstesd",
    password: "lqqWcMzq",
    database: "nilstesd",
    debug: false,
    multipleStatements: true

});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

afterAll(() => {
  pool.end();
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Sveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

test("Update one person in database", done => {
    function callback(status, data) {
        console.log("Test callback: status = " + status + ", data.length=" + JSON.stringify(data));

        personDao.getOne(1, callback2);
    }

    function callback2(status, data) {

        expect(data[0].navn).toBe("Nils Nilsen");
        expect(data[0].alder).toBe(34);
        expect(data[0].adresse).toBe("Gata 3");

        done();

    }
    personDao.updateOne(
        {navn: "Nils Nilsen", alder: 34, adresse: "Gata 3"},
        1,
        callback);

});

test("delete one person in database", done => {
    var antPers;
    function callback(status, data) {
        console.log("Test callback: status = " + status + ", data.length=" + JSON.stringify(data));


        antPers = data.length;
        personDao.deleteOne(1, callback3);

    }

    function callback3(status, data) {
        personDao.getAll(callback4);
    }


    function callback4(status, data){

        expect(data.length).toBe(antPers-1);
        done();
    }

    personDao.getAll(callback);

});
